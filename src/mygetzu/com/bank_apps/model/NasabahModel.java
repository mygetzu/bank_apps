/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygetzu.com.bank_apps.model;

/**
 *
 * @author NAD
 */
public class NasabahModel extends AccountModel{
    private TabunganModel tabungan;
    
    public NasabahModel(int id, String nama, String username, String password, int role_user, TabunganModel tab) {
        super(id, nama, username, password, role_user);
        this.tabungan = tab;
    }

    public TabunganModel getTabungan() {
        return tabungan;
    }

    public void setTabungan(TabunganModel tabungan) {
        this.tabungan = tabungan;
    }
    
}
