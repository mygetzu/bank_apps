/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygetzu.com.bank_apps.model;

/**
 *
 * @author mygetzu
 */
public class AntrianModel {
    private int no_antrian;
    private NasabahModel nasabah;

    public AntrianModel(int no_antrian, NasabahModel nasabah) {
        this.no_antrian = no_antrian;
        this.nasabah = nasabah;
    }

    public int getNo_antrian() {
        return no_antrian;
    }

    public void setNo_antrian(int no_antrian) {
        this.no_antrian = no_antrian;
    }

    public NasabahModel getNasabah() {
        return nasabah;
    }

    public void setNasabah(NasabahModel nasabah) {
        this.nasabah = nasabah;
    }

    
    
    
}
