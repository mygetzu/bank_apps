/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygetzu.com.bank_apps.model;

import java.util.ArrayList;
import java.util.List;
import mygetzu.com.bank_apps.config.Variable;

/**
 *
 * @author NAD
 */
public class StarterData extends Variable{
    public static List<TellerModel> starterTeller   = new ArrayList<TellerModel>();
    public static List<NasabahModel> starterNasabah = new ArrayList<NasabahModel>();
    public static List<AntrianModel> antrian        = new ArrayList<AntrianModel>();
    
    public StarterData() {    
    }
    
    public static void setAllStarterData() {
        for (int i = 0; i < 5; i++) {
            starterTeller.add(new TellerModel(
                    i+1, "Teller " + (i+1), "teller" + (i+1), "teller" + (i+1), Variable.ROLE_TELLER
            ));
        }
        for (int i = 0; i < 10; i++) {
            starterNasabah.add(new NasabahModel(
                    i+1, "Nasabah " + (i+1), "nasabah" + (i+1), "nasabah" + (i+1), Variable.ROLE_NASABAH, new TabunganModel("M Beny P" + i, (1000000 + (i * 1000)), (1400019980 + i), 123456)
            ));
        }
        for (int i = 0; i < 10; i++) {
            antrian.add(new AntrianModel(i+1, starterNasabah.get(i)));
        }    
    }

    public List<TellerModel> getStarterTeller() {
        return starterTeller;
    }

    public void setStarterTeller(List<TellerModel> starterTeller) {
        this.starterTeller = starterTeller;
    }

    public List<NasabahModel> getStarterNasabah() {
        return starterNasabah;
    }

    public void setStarterNasabah(List<NasabahModel> starterNasabah) {
        this.starterNasabah = starterNasabah;
    }

    public static List<AntrianModel> getAntrian() {
        return antrian;
    }

    public static void setAntrian(List<AntrianModel> antrian) {
        StarterData.antrian = antrian;
    }
    
    public void addStarterTeller(TellerModel tellerModel) {
        synchronized (starterTeller) {
            starterTeller.add(tellerModel);
            starterTeller.notifyAll();
        }
    }
    
    public void addStarterNasabah(NasabahModel nasabahModel) {
        synchronized (starterNasabah) {
            starterNasabah.add(nasabahModel);
            starterNasabah.notifyAll();
        }
    }
    
    
}
