/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygetzu.com.bank_apps.model;

/**
 *
 * @author NAD
 */
public class AccountModel {
    public int id;
    public String nama;
    public String username;
    public String password;
    public int role_user;

    public AccountModel(int id, String nama, String username, String password, int role_user) {
        this.id = id;
        this.nama = nama;
        this.username = username;
        this.password = password;
        this.role_user = role_user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRole_user() {
        return role_user;
    }

    public void setRole_user(int role_user) {
        this.role_user = role_user;
    }
    
    
}
