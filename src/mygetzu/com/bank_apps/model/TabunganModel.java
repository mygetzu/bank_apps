/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygetzu.com.bank_apps.model;

/**
 *
 * @author NAD
 */
public class TabunganModel {
    private String  nama_akun;
    private double  saldo;
    private int     no_rekening;
    private int     pin;

    public TabunganModel(String nama_akun, double saldo, int no_rekening, int pin) {
        this.nama_akun = nama_akun;
        this.saldo = saldo;
        this.no_rekening = no_rekening;
        this.pin = pin;
    }

    public String getNama_akun() {
        return nama_akun;
    }

    public void setNama_akun(String nama_akun) {
        this.nama_akun = nama_akun;
    }

    public int getNo_rekening() {
        return no_rekening;
    }

    public void setNo_rekening(int no_rekening) {
        this.no_rekening = no_rekening;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public int getPin() {
        return pin;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }
    
    
}
