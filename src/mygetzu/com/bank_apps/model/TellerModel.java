/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygetzu.com.bank_apps.model;

/**
 *
 * @author NAD
 */
public class TellerModel extends AccountModel {
    public TellerModel(int id, String nama, String username, String password, int role_user) {
        super(id, nama, username, password, role_user);
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
}
