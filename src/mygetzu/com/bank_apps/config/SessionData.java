/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygetzu.com.bank_apps.config;

import mygetzu.com.bank_apps.model.AntrianModel;
import mygetzu.com.bank_apps.model.TabunganModel;

/**
 *
 * @author mygetzu
 */
public class SessionData {
    private static TabunganModel u_tabungan;
    private static AntrianModel bank_antrian;

    public SessionData() {
    }

    public static TabunganModel getU_tabungan() {
        return u_tabungan;
    }

    public static void setU_tabungan(TabunganModel u_tabungan) {
        SessionData.u_tabungan = u_tabungan;
    }

    public static AntrianModel getBank_antrian() {
        return bank_antrian;
    }

    public static void setBank_antrian(AntrianModel bank_antrian) {
        SessionData.bank_antrian = bank_antrian;
    }
    
    
}
