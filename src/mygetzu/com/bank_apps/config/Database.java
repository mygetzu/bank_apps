/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygetzu.com.bank_apps.config;
import java.sql.*;

/**
 *
 * @author SMK2N KRASAAN
 */
public class Database {
    //variable koneksi
    Connection kon;
    
    private final String driver = "com.mysql.jdbc.Driver";
    private final String url = "jdbc:mysql://localhost:3306/db_posbeny_xiirpl1";
    private final String user = "root";
    private final String password = "P@ssw0rd|mysql";
    
    //METHOD
       public void koneksi(){
           try {
               Class.forName(driver);
               kon=DriverManager.getConnection(url,user,password);
               System.out.println("berhasil yo !");
           } catch( Exception e){
                System.out.println("Error:\nKoneksi database gagal !");
           }
           
       }
       
       public ResultSet ambilData (String SQL) {
           try {
               Statement st=kon.createStatement();
               return st.executeQuery(SQL);
           } catch (Exception e) {
               System.out.println("Error:\nData cek gagal di akses!");
               return null;
           }
       }
       
       public void aksi (String SQL) {
           try {
               Statement st=kon.createStatement();
               st.executeUpdate(SQL);
               System.out.println("Success:\nAksi sukses!");
           } catch (Exception e) {
               System.out.println("Error:\nAksi gagal di akses!");
           }
       }
}
