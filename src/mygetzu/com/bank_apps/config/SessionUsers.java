/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygetzu.com.bank_apps.config;

import mygetzu.com.bank_apps.model.NasabahModel;
import mygetzu.com.bank_apps.model.TabunganModel;

/**
 *
 * @author mygetzu
 */
public class SessionUsers {
    private static int u_id;
    private static String u_nama;
    private static String u_username;
    private static String u_password;
    private static int u_role;
    
    public SessionUsers() {
        
    }

    public static int getU_id() {
        return u_id;
    }

    public static void setU_id(int u_id) {
        SessionUsers.u_id = u_id;
    }

    public static String getU_nama() {
        return u_nama;
    }

    public static void setU_nama(String u_nama) {
        SessionUsers.u_nama = u_nama;
    }

    public static String getU_username() {
        return u_username;
    }

    public static void setU_username(String u_username) {
        SessionUsers.u_username = u_username;
    }

    public static String getU_password() {
        return u_password;
    }

    public static void setU_password(String u_password) {
        SessionUsers.u_password = u_password;
    }

    public static int getU_role() {
        return u_role;
    }

    public static void setU_role(int u_role) {
        SessionUsers.u_role = u_role;
    }
    
}
